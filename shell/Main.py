#!/usr/bin/python
#coding: utf-8
import sys
import getpass
import requests
import json

def pturl(host, api, action, args):
    if host[0:4] != "http":
        ret = "http://%s/api.php?p=%sAPI&action=%s"%(host, api, action)
    else:
        ret = "%s/api.php?p=%sAPI&action=%s"%(host, api, action)
    if args:
        for arg in args:
            ret = "%s&%s"%(ret, arg)
    return ret

class CommandRegistry:

    commands = {}

    def addCommand(self, keyword, function):
        self.commands[keyword] = function

    def callCmd(self, keyword, args):
        if keyword not in self.commands.iterkeys():
            return "Unknown command %s"%(keyword)
        else:
            return self.commands[keyword](args)

def login():
    if not getState("host"):
        return "Host not set"
    if not getState("user") or not getState("password"):
        return "User not set"
    targetArgs = ["user=%s"%(getState("user")), "password=%s"%(getState("password"))]
    target = pturl(getState("host"), "Login", "login", targetArgs)
    result = requests.get(target)
    if result.status_code != 200:
        return "%s: %s"%(result.status_code, result.text)
    try:
        data = json.loads(result.text)
        if "token" in data: setState("token", data["token"])
    except Exception as e:
        print("Unable to parse response: %s"%(e))
    return result.text
def loginCmd(args):
    return login()

def openCmd(args):
    setState("host", args[0])
    setState("user", None)
    setState("password", None)
    setState("token", None)
    return "Host set"
def userCmd(args):
    if len(args) < 2:
        setState("user", args[0])
        setState("password", getpass.getpass())
    else:
        setState("user", args[0])
        setState("password", args[1])
    return login()
def tokenCmd(args):
    token = getState("token")
    if not token and user:
        login()
    return getState("token")
def callCmd(args):
    if not getState("host"):
        return "Host not set"
    if len(args) < 2:
        return "Missing parameters"
    api = args[0]
    action = args[1]
    apiargs = args[2:]
    apiargs.append("token=%s"%(getState("token")))
    result = requests.get(pturl(getState("host"), api, action, apiargs))
    if result.status_code != 200:
        return "%s: %s"%(result.status_code, result.text)
    try:
        data = json.loads(result.text)
        if "token" in data: setState("token", data["token"])
    except Exception as e:
        print("Unable to parse response: %s"%(e))
    return result.text
def exitCmd(args):
    exit()
def helpCmd(args):
    return """List of commands:
  open <url>: set host
  user <username> [<password]: set credential data
  call <API> <action> <key=value> ...: run an API call
         It automatically adds the 'API' suffix to API name
  token: print current auth token
  newtoken: request for a new auth token
  help: show this help
  exit: close the program"""

cmdRegistry = CommandRegistry()
cmdRegistry.addCommand("open", openCmd)
cmdRegistry.addCommand("user", userCmd)
cmdRegistry.addCommand("call", callCmd)
cmdRegistry.addCommand("token", tokenCmd)
cmdRegistry.addCommand("newtoken", loginCmd)
cmdRegistry.addCommand("exit", exitCmd)
cmdRegistry.addCommand("help", helpCmd)

state = {"host": None, "user": None, "password": None, "token": None}
def setState(var, value):
    global state
    state[var] = value
def getState(var):
    global state
    if var in state:
        return state[var]
    return None

def prompt():
    p = ""
    if getState("user"):
        p = getState("user")
    else:
        p = "???"
    if getState("host"):
        p = "%s@%s"%(p, getState("host"))
    else:
        p = "%s@???"%(p)
    return "%s>"%(p)
        

print "Welcome to Pasteque. If you are lost, type 'help'."

if len(sys.argv) == 2:
    # Read script file
    f = open(sys.argv[1])
    for line in f:
        line = line[:-1]
        print line
        args = line.split(" ")
        print(cmdRegistry.callCmd(args[0], args[1:]))
    f.close

while True:
    line = raw_input(prompt())
    args = line.split(" ")
    print(cmdRegistry.callCmd(args[0], args[1:]))

